#include <iostream>
#include "MPIReductor/MMPIReductor.h"

int main(int argc, char * argv[]) {

    MPI::Init(argc, argv);

    // TYPE = int, float or double
    int size = 40;
    double *tab;
    MMPIReductor<double> mmpiReductor(size);

    // creation of global array on Master only
    if (mmpiReductor.is_master()) {
        // initialize array
        tab = new double[size];
        size_t index = 0;
        for (int cpu = 0; cpu < mmpiReductor.max_cpus(); ++cpu) {
            for (size_t i = 0; i < mmpiReductor.local_sizes()[cpu]; ++i) {
                tab[index++] = 2;
            }
        }

        mmpiReductor << "GLOBAL_ARRAY=";
        for (int i = 0; i < size; ++i) mmpiReductor << tab[i] << " ";
        mmpiReductor << MMPI::endl;
    }

    // all CPUs do the job
    mmpiReductor.process_and_print(tab, MPI::PROD);


    if (mmpiReductor.is_master()) {
        delete[] tab;
    }

    mmpiReductor.finalize();
    MPI::Finalize();
}