//
// Created by pviolette on 16/11/18.
//

#ifndef MPI_REDUCTOR_MPIREDUCTOR_H
#define MPI_REDUCTOR_MPIREDUCTOR_H

#include <vector>
#include <algorithm>

#include "../mmpi/mmpi.h"
#include "../utils/ansi_colors.h"

template<class TYPE>
class MMPIReductor : public MMPI {

private:
    int _array_size;
    std::vector<int> _local_sizes;

    void computeLocalSizes() {
        int eltByCpu = _array_size / max_cpus();
        for(auto& e : _local_sizes){
            e = eltByCpu;
        }
        int left = _array_size % max_cpus();
        while(left > 0){
            for(int i = 0; i < max_cpus() && left > 0; ++i, --left){
                _local_sizes[i]++;
            }
        }
    }

    TYPE compute(TYPE *local_array, int localSize, const MPI::Op &op) {
        if(op == MPI::SUM){
            return std::accumulate(local_array, local_array + localSize, 0);
        }else if(op == MPI::PROD){
            return std::accumulate(local_array, local_array + localSize, 1, std::multiplies<TYPE>());
        }
        else if(op == MPI::MIN){
            return *(std::min_element(local_array, local_array + localSize));
        }else if(op == MPI::MAX){
            return *(std::max_element(local_array, local_array + localSize));
        }else{
            throw std::runtime_error("Unsupported operation");
        }
    }

public:
    explicit MMPIReductor(int _array_size) : MMPI(), _array_size(_array_size), _local_sizes(
            static_cast<unsigned long>(this->max_cpus())) {
        this->computeLocalSizes();
    }

    TYPE process(TYPE *array, const MPI::Op &op) {
        TYPE local_result;
        if(this->is_master()){
            // Send array parts
            int start = _local_sizes[0];
            for (int i = 1; i < this->max_cpus(); ++i) {
                this->remote(i);
                send(_local_sizes[i]); //Send size of the array so slaves can allocate memory
                send(array + start, _local_sizes[i]); //Send array
                start += _local_sizes[i];
            }

            local_result = this->compute(array, _local_sizes[0], op);

        }else{
            TYPE *local_data;

            this->remote(0);

            int local_data_size;
            //Receive size of the array
            this->recv(local_data_size);

            // each processor creates its local data
            local_data = new TYPE[local_data_size];

            //Receive array
            this->recv(local_data, local_data_size);

            local_result = this->compute(local_data, local_data_size, op);

            delete[] local_data;
        }
        if(m_verbose) *this << "local_result=" << local_result << MMPI::endl;

        MPI::COMM_WORLD.Barrier();
        TYPE final_result;

        this->reduce(local_result, final_result, op);

        return final_result;
    }

    TYPE process_and_print(TYPE *array, const MPI::Op &op) {
        TYPE result = this->process(array, op);

        if(this->is_master()){
            *this << ANSI_COLOR_GREEN << "Final Result = " << result << ANSI_COLOR_RESET << MMPI::endl;
        }
        return result;
    }

    const std::vector<int>& local_sizes() const{ return _local_sizes; }
};


#endif //MPI_REDUCTOR_MPIREDUCTOR_H
